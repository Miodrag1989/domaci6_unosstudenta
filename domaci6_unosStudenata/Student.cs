﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci6_unosStudenata
{
    class Student
    {
        protected string ime;
        protected string prezime;
        private int brojIndeksa;
        private float prosecnaOcena;
        private string godinaStudija;

        public Student(string ime, string prezime, int brojIndeksa, float prosecnaOcena, string godinaStudija)
        {
            this.ime = ime;
            this.prezime = prezime;
            this.brojIndeksa = brojIndeksa;
            this.prosecnaOcena = prosecnaOcena;
            this.godinaStudija = godinaStudija;
        }

        public virtual float Uspeh { get { return this.prosecnaOcena; } }

        public override string ToString()
        {
            return "Ime: " + this.ime + " Prezime: " + this.prezime + " Uspeh: " + this.Uspeh;
        }
    }
}
