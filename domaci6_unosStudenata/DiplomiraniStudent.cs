﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace domaci6_unosStudenata
{
    class DiplomiraniStudent : Student
    {
        private int ocenaSaDiplomskog;

        public DiplomiraniStudent(string ime, string prezime, int brojIndeksa, float prosecnaOcena, string godinaStudija, int ocenaSaDiplomskog)
        : base(ime, prezime, brojIndeksa, prosecnaOcena, godinaStudija)
        {
            this.ocenaSaDiplomskog = ocenaSaDiplomskog;
        }

        public override float Uspeh { get { return (base.Uspeh + this.ocenaSaDiplomskog) / 2; } }

        public override string ToString()
        {
            return "Ime: " + base.ime + " Prezime: " + base.prezime + " Uspeh: " + this.Uspeh;
        }
    }

    
}
