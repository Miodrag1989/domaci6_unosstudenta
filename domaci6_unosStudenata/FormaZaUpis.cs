﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace domaci6_unosStudenata
{
    public partial class FormaZaUpis : Form
    {
        Student novistudent;
        public FormaZaUpis()
        {
            InitializeComponent();
            textBoxOcenaSaDiplomskog.Enabled = false;
            
        }

        public void Provera()
        {
            if(textBoxIme.Text == null || textBoxIme.Text.Equals(""))
            {
                MessageBox.Show("Potrebno je uneti ime studenta", "Greska!");
                return;
            }
            else if(textBoxPrezime.Text == null || textBoxPrezime.Text.Equals(""))
            {
                MessageBox.Show("Potrebno je uneti prezime studenta", "Greska!");
                return;
            }
            else if(textBoxbrIndexa.Text == null || textBoxbrIndexa.Text.Equals(""))
            {
                MessageBox.Show("Potrebno je uneti broj indexa studenta", "Greska!");
                return;
            }
            else if (textBoxprOcena.Text == null || textBoxprOcena.Text.Equals(""))
            {
                MessageBox.Show("Potrebno je uneti prosecnu ocenu studenta", "Greska!");
                return;
            }
            
            if(!checkBoxdiplomirao.Checked)
            {
                if (listBoxGodinaStudija.SelectedIndex == -1)
                {
                    MessageBox.Show("Potrebno je izabrati godinu studije studenta", "Greska!");
                    return;
                }
                novistudent = new Student(textBoxIme.Text, textBoxPrezime.Text, Convert.ToInt32(textBoxbrIndexa.Text), Convert.ToSingle(textBoxprOcena.Text),listBoxGodinaStudija.Text);
            }
            else
            {
                if(textBoxOcenaSaDiplomskog.Text == null || textBoxOcenaSaDiplomskog.Text.Equals(""))
                {
                    MessageBox.Show("Potrebno je ubaciti ocenu sa diplomskog ispita", "Greska!");
                    return;
                }
                else
                {
                    novistudent = new DiplomiraniStudent(textBoxIme.Text, textBoxPrezime.Text, Convert.ToInt32(textBoxbrIndexa.Text), Convert.ToSingle(textBoxprOcena.Text), "diplomirao", Convert.ToInt32(textBoxOcenaSaDiplomskog.Text));
                }
            }
            
            
        }

        public void Prikaz()
        {
            if(novistudent is DiplomiraniStudent)
                MessageBox.Show(novistudent.ToString(), "Diplomirani Student");
            else
                MessageBox.Show(novistudent.ToString(), "Student");
        }

        private void buttonUpisi_Click(object sender, EventArgs e)
        {
            Provera();
            Prikaz();
            Close();
        }

        private void checkBoxdiplomirao_CheckedChanged(object sender, EventArgs e)
        {
            if (checkBoxdiplomirao.Checked)
            {
                textBoxOcenaSaDiplomskog.Enabled = true;
                listBoxGodinaStudija.Enabled = false;
            }
            else
            {
                textBoxOcenaSaDiplomskog.Enabled = false;
                listBoxGodinaStudija.Enabled = true;
            }
        }

        private void buttonOtkazi_Click(object sender, EventArgs e)
        {
            Close();
        }
    }
}
