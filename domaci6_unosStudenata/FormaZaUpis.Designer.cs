﻿namespace domaci6_unosStudenata
{
    partial class FormaZaUpis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.labelIme = new System.Windows.Forms.Label();
            this.labelPrezime = new System.Windows.Forms.Label();
            this.labelbrindexa = new System.Windows.Forms.Label();
            this.labelprosecnaocena = new System.Windows.Forms.Label();
            this.labelgodinastudija = new System.Windows.Forms.Label();
            this.labelocenasadiplomskog = new System.Windows.Forms.Label();
            this.textBoxIme = new System.Windows.Forms.TextBox();
            this.textBoxPrezime = new System.Windows.Forms.TextBox();
            this.textBoxbrIndexa = new System.Windows.Forms.TextBox();
            this.textBoxOcenaSaDiplomskog = new System.Windows.Forms.TextBox();
            this.textBoxprOcena = new System.Windows.Forms.TextBox();
            this.listBoxGodinaStudija = new System.Windows.Forms.ListBox();
            this.checkBoxdiplomirao = new System.Windows.Forms.CheckBox();
            this.buttonUpisi = new System.Windows.Forms.Button();
            this.buttonOtkazi = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // labelIme
            // 
            this.labelIme.AutoSize = true;
            this.labelIme.Location = new System.Drawing.Point(29, 24);
            this.labelIme.Name = "labelIme";
            this.labelIme.Size = new System.Drawing.Size(27, 13);
            this.labelIme.TabIndex = 0;
            this.labelIme.Text = "Ime:";
            // 
            // labelPrezime
            // 
            this.labelPrezime.AutoSize = true;
            this.labelPrezime.Location = new System.Drawing.Point(29, 78);
            this.labelPrezime.Name = "labelPrezime";
            this.labelPrezime.Size = new System.Drawing.Size(47, 13);
            this.labelPrezime.TabIndex = 1;
            this.labelPrezime.Text = "Prezime:";
            // 
            // labelbrindexa
            // 
            this.labelbrindexa.AutoSize = true;
            this.labelbrindexa.Location = new System.Drawing.Point(29, 141);
            this.labelbrindexa.Name = "labelbrindexa";
            this.labelbrindexa.Size = new System.Drawing.Size(63, 13);
            this.labelbrindexa.TabIndex = 2;
            this.labelbrindexa.Text = "Broj Indexa:";
            // 
            // labelprosecnaocena
            // 
            this.labelprosecnaocena.AutoSize = true;
            this.labelprosecnaocena.Location = new System.Drawing.Point(29, 209);
            this.labelprosecnaocena.Name = "labelprosecnaocena";
            this.labelprosecnaocena.Size = new System.Drawing.Size(90, 13);
            this.labelprosecnaocena.TabIndex = 3;
            this.labelprosecnaocena.Text = "Prosecna Ocena:";
            // 
            // labelgodinastudija
            // 
            this.labelgodinastudija.AutoSize = true;
            this.labelgodinastudija.Location = new System.Drawing.Point(29, 313);
            this.labelgodinastudija.Name = "labelgodinastudija";
            this.labelgodinastudija.Size = new System.Drawing.Size(77, 13);
            this.labelgodinastudija.TabIndex = 4;
            this.labelgodinastudija.Text = "Godina studija:";
            // 
            // labelocenasadiplomskog
            // 
            this.labelocenasadiplomskog.AutoSize = true;
            this.labelocenasadiplomskog.Location = new System.Drawing.Point(29, 473);
            this.labelocenasadiplomskog.Name = "labelocenasadiplomskog";
            this.labelocenasadiplomskog.Size = new System.Drawing.Size(109, 13);
            this.labelocenasadiplomskog.TabIndex = 5;
            this.labelocenasadiplomskog.Text = "Ocena sa diplomskog";
            // 
            // textBoxIme
            // 
            this.textBoxIme.Location = new System.Drawing.Point(32, 40);
            this.textBoxIme.Name = "textBoxIme";
            this.textBoxIme.Size = new System.Drawing.Size(212, 20);
            this.textBoxIme.TabIndex = 6;
            // 
            // textBoxPrezime
            // 
            this.textBoxPrezime.Location = new System.Drawing.Point(32, 105);
            this.textBoxPrezime.Name = "textBoxPrezime";
            this.textBoxPrezime.Size = new System.Drawing.Size(212, 20);
            this.textBoxPrezime.TabIndex = 7;
            // 
            // textBoxbrIndexa
            // 
            this.textBoxbrIndexa.Location = new System.Drawing.Point(32, 168);
            this.textBoxbrIndexa.Name = "textBoxbrIndexa";
            this.textBoxbrIndexa.Size = new System.Drawing.Size(212, 20);
            this.textBoxbrIndexa.TabIndex = 8;
            // 
            // textBoxOcenaSaDiplomskog
            // 
            this.textBoxOcenaSaDiplomskog.Location = new System.Drawing.Point(32, 505);
            this.textBoxOcenaSaDiplomskog.Name = "textBoxOcenaSaDiplomskog";
            this.textBoxOcenaSaDiplomskog.Size = new System.Drawing.Size(212, 20);
            this.textBoxOcenaSaDiplomskog.TabIndex = 9;
            // 
            // textBoxprOcena
            // 
            this.textBoxprOcena.Location = new System.Drawing.Point(32, 238);
            this.textBoxprOcena.Name = "textBoxprOcena";
            this.textBoxprOcena.Size = new System.Drawing.Size(212, 20);
            this.textBoxprOcena.TabIndex = 10;
            // 
            // listBoxGodinaStudija
            // 
            this.listBoxGodinaStudija.FormattingEnabled = true;
            this.listBoxGodinaStudija.Items.AddRange(new object[] {
            "PRVA",
            "DRUGA",
            "TRECA",
            "CETVRTA"});
            this.listBoxGodinaStudija.Location = new System.Drawing.Point(32, 339);
            this.listBoxGodinaStudija.Name = "listBoxGodinaStudija";
            this.listBoxGodinaStudija.Size = new System.Drawing.Size(212, 56);
            this.listBoxGodinaStudija.TabIndex = 11;
            // 
            // checkBoxdiplomirao
            // 
            this.checkBoxdiplomirao.AutoSize = true;
            this.checkBoxdiplomirao.Location = new System.Drawing.Point(32, 435);
            this.checkBoxdiplomirao.Name = "checkBoxdiplomirao";
            this.checkBoxdiplomirao.Size = new System.Drawing.Size(75, 17);
            this.checkBoxdiplomirao.TabIndex = 12;
            this.checkBoxdiplomirao.Text = "Diplomirao";
            this.checkBoxdiplomirao.UseVisualStyleBackColor = true;
            this.checkBoxdiplomirao.CheckedChanged += new System.EventHandler(this.checkBoxdiplomirao_CheckedChanged);
            // 
            // buttonUpisi
            // 
            this.buttonUpisi.Location = new System.Drawing.Point(32, 574);
            this.buttonUpisi.Name = "buttonUpisi";
            this.buttonUpisi.Size = new System.Drawing.Size(75, 23);
            this.buttonUpisi.TabIndex = 13;
            this.buttonUpisi.Text = "Upisi";
            this.buttonUpisi.UseVisualStyleBackColor = true;
            this.buttonUpisi.Click += new System.EventHandler(this.buttonUpisi_Click);
            // 
            // buttonOtkazi
            // 
            this.buttonOtkazi.Location = new System.Drawing.Point(169, 574);
            this.buttonOtkazi.Name = "buttonOtkazi";
            this.buttonOtkazi.Size = new System.Drawing.Size(75, 23);
            this.buttonOtkazi.TabIndex = 14;
            this.buttonOtkazi.Text = "Otkazi";
            this.buttonOtkazi.UseVisualStyleBackColor = true;
            this.buttonOtkazi.Click += new System.EventHandler(this.buttonOtkazi_Click);
            // 
            // FormaZaUpis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(374, 603);
            this.Controls.Add(this.buttonOtkazi);
            this.Controls.Add(this.buttonUpisi);
            this.Controls.Add(this.checkBoxdiplomirao);
            this.Controls.Add(this.listBoxGodinaStudija);
            this.Controls.Add(this.textBoxprOcena);
            this.Controls.Add(this.textBoxOcenaSaDiplomskog);
            this.Controls.Add(this.textBoxbrIndexa);
            this.Controls.Add(this.textBoxPrezime);
            this.Controls.Add(this.textBoxIme);
            this.Controls.Add(this.labelocenasadiplomskog);
            this.Controls.Add(this.labelgodinastudija);
            this.Controls.Add(this.labelprosecnaocena);
            this.Controls.Add(this.labelbrindexa);
            this.Controls.Add(this.labelPrezime);
            this.Controls.Add(this.labelIme);
            this.Name = "FormaZaUpis";
            this.Text = "Student";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelIme;
        private System.Windows.Forms.Label labelPrezime;
        private System.Windows.Forms.Label labelbrindexa;
        private System.Windows.Forms.Label labelprosecnaocena;
        private System.Windows.Forms.Label labelgodinastudija;
        private System.Windows.Forms.Label labelocenasadiplomskog;
        private System.Windows.Forms.TextBox textBoxIme;
        private System.Windows.Forms.TextBox textBoxPrezime;
        private System.Windows.Forms.TextBox textBoxbrIndexa;
        private System.Windows.Forms.TextBox textBoxOcenaSaDiplomskog;
        private System.Windows.Forms.TextBox textBoxprOcena;
        private System.Windows.Forms.ListBox listBoxGodinaStudija;
        private System.Windows.Forms.CheckBox checkBoxdiplomirao;
        private System.Windows.Forms.Button buttonUpisi;
        private System.Windows.Forms.Button buttonOtkazi;
    }
}

